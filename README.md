# Veilles Technos

Projet de Veille  Techno

## Sujets

  - Files d'attente : Gearman, **RabbitMQ**, NATS, Kafka => CamilleA
  - Redis
  - Gestion du cache => Tarik, CamilleT
  - RegExp
  - Docker => Romain, Jim
  - CI/CD => DevOps via Gitlab => Romain, Jim, Barth
  - Gestion des git (GitFlow, GitMoji, branche unique, ...) => Serigne, Mustapha
  - Gestion de la documentation technique => Safik, Amine
  - Tests de charge
  - NoSQL => Aurélien
  - Répartition de Charge
  - Blockchain => Jason, Damien
  - Internationalisation/localisation => Henrique, Mohammad



  ## Objectifs : 

  1h : présentation du sujet, démo, Q&A
